/**
 * Created by skolomoychenko on 24.08.2016.
 */
var lsit_galarys=[];

    var objs={
        data:{},
        add_or_remove: function (id, fname) {
            if (fname in this.data){
                var index = this.data[fname].indexOf(id);
                if (index === -1){
                    this.add(id, fname);
                    return true
                }else{
                    this.remove(id, fname);
                    return false;
                }
            }else{
                this.add(id, fname);
                return true;
            }
        },
        add: function (id, fname) {
            if (fname in this.data){
                var index = this.data[fname].indexOf(id);
                if (index === -1) {
                    this.data[fname].push(id);
                }
            }else{
                this.data[fname]=[id];
            }
        },
        remove: function (id, fname) {
            var index = this.data[fname].indexOf(id);
            this.data[fname].splice(index, 1);
        },
        get_galary: function (fname) {
            if (fname in this.data) {
                return this.data[fname]
            }else{
                return []
            }
        }
    };



    function select(el, id, fname) {
        var result = objs.add_or_remove(id.toString(), fname);
        console.log(result);
        if (result){
            $(el).closest('.laporem_lslide').addClass('checked')
        }else{
            $(el).closest('.laporem_lslide').removeClass('checked')
        }
        objs.get_galary()
    }

    function select_all_laporem(el, fname) {
        var slides = $('.laporem_lslide', $(el).closest('.laporem_field'));
        if (slides.length === objs.get_galary(fname).length){
            slides.each(function () {
                objs.remove(this.getAttribute('data-id'), fname);
                $(this).removeClass('checked')
            });
        }else{
            slides.each(function () {
                objs.add(this.getAttribute('data-id'), fname);
                $(this).addClass('checked')
            });
        }
    }

    function delete_laporem_select(classname, fname) {
        var ids = objs.get_galary(fname);
        if (ids.length > 0) {
            if (confirm("Удалить выделенное?")) {
                $('.loader').fadeIn();
                $.ajax({
                    type: "POST",
                    url: "/laporem/delete_select/",
                    data: {ids: ids, 'classname': classname},
                    success: function (data) {
                        $('#iG-'+fname+' .laporem_lslide.checked').remove();
                        for (var i = 0; i < ids.length; i++) {
                          objs.remove(ids[i], fname);
                        }
                        $(window).resize();
                        $('.loader').fadeOut();
                    },
                    error: function (xhr, str) {
                        alert('Возникла ошибка: ' + xhr.responseCode);
                    }
                })
            }
        }
    }


    function delete_laporem_obj(classname, id, el) {
        if (confirm("Удалить?")) {
            $('.loader').fadeIn();
            $.ajax({
                type: "GET",
                url: "/laporem/" + classname + "/" + id + "/delete",
                success: function (data) {
                    $(el).closest('.laporem_lslide').remove();
                    $(window).resize();
                    $('.loader').fadeOut();
                },
                error: function (xhr, str) {
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            })
        }
    }


    function loadfiles(element ,files){
            var form = new FormData($('form').get(0));
            $('.loader').fadeIn();
            if (files){
                var name = $('.file', $(element).closest('.laporem_field')).attr('name');
                for (var i in files) {
                    form.append(name, files[i]);
                }
            }
            $.ajax({
                type: "POST",
                url: "/laporem/loadfiles/",
                cache: false,
                processData: false,
                contentType: false,
                data: form,
                success: function(data) {
                    var gal = $('.IG', $(element).closest('.laporem_field'));
                    gal.append(data);
                    var field = gal.data('field');
                    setTimeout(function () {
                        $(window).resize();
                        $('.loader').fadeOut();
                    }, 1000);
                    $('form').get(0).reset();
                },
                error:  function(xhr, str){
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            });

    }

    function move_laporem(element) {
        var gal = $(element).closest('.IG');
        var list = [];
        $('.loader', element).show();
        $('.laporem_lslide', gal).each(function () {
            list.push($(this).data('id'))
        });
        $.get("/laporem/move/", {
            list: list,
            laporem_model_file: gal.data('laporem-model-file')
        },function () {
            $('.loader', element).hide();
        })
    }




    $(document).ready(function(){

        $('.file').change(function() {
            loadfiles(this);
        });

     function handleFileSelect(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        var files = evt.dataTransfer.files;
        loadfiles(this, files);
      }

      function handleDragOver(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        $(this).addClass('dragover')
      }

      function handleDragLeave(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        $(this).removeClass('dragover')
      }


        $('.dragzone').each(function (dropZone) {
            this.addEventListener('dragover', handleDragOver, false);
            this.addEventListener('drop', handleFileSelect, false);
            this.addEventListener('dragleave', handleDragLeave, false);
            this.addEventListener('drop', handleDragLeave, false);
        })

      $('.IG').sortable({
           cursor: '-webkit-grabbing',
           opacity:0.5,
           distance: 20,
           placeholder: 'emptySpace',
           remove: function (event, ui) {
               $(ui.item).toggleClass('lets_moved')
           },
           update: function( event, ui ) {
                move_laporem(ui.item)
           }
       });


    });

